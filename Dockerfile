# SatNOGS Kaitai Struct Docker image
#
# Copyright (C) 2018-2021 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG OPENJDK_IMAGE_TAG=15-buster
FROM openjdk:${OPENJDK_IMAGE_TAG}
MAINTAINER SatNOGS project <dev@satnogs.org>

ARG KAITAI_VERSION

WORKDIR /workdir/

# Install 'kaitai-struct-compiler'
RUN apt-get update \
	&& curl -sLO https://github.com/kaitai-io/kaitai_struct_compiler/releases/download/${KAITAI_VERSION}/kaitai-struct-compiler_${KAITAI_VERSION}_all.deb \
	&& apt-get install -qy ./kaitai-struct-compiler_${KAITAI_VERSION}_all.deb \
	&& rm kaitai-struct-compiler_${KAITAI_VERSION}_all.deb \
	&& rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["/usr/bin/ksc"]
